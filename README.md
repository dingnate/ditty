# ditty

## 项目介绍
ditty是基于netty、fastjson实现的一款极致简约web restful框架。

## 使用说明

### 编写Controller类

```java
/**
 * 注册路由信息的类，类上需要有{@link Controller}注解
 * @author dingnate
 *
 */
@ActionInterceptor(ControllerInterceptor.class)
@ActionMapping(actionKey = "/controller")
@Controller
public class DemoController {
	/**
	 * httpMethod指定http的请求方式
	 * argName指定参数名可以在web访问的时候使用<br>
	 * actionKey:控制器的actionKey和方法的actionKey确定该方法的唯一路由路径<br>
	 * 不指定方法的actionKey默认为方法名称
	 * @param b
	 * @return
	 */
	@ActionMapping(httpMethod = HttpKit.METHOD_POST, actionKey = "method1", argNames = { "b" })
	public AxBean method1(BxBean b) {
		System.out.println(getClass().getName() + ".method1(BBean)");
		return new AxBean(1, true, "A", b);
	}

	/**
	 * 本方法的拦截器不仅包含在本方法上声明的拦截器，还包含上层控制器的拦截器、全局拦截器
	 * @param a
	 * @return
	 */
	@ActionInterceptor(MethodInterceptor.class)
	@ActionMapping(argNames = { "a" })
	public int method2(int a) {
		System.out.println(getClass().getName() + ".method2(int)");
		return a;
	}

	/**
	 * 这个方法清除了上层拦截器，保留本方法的拦截器<br>
	 * 通过web访问时，参数名可以用下表代替，比如 “a”->"0"
	 * @param a
	 * @return
	 */
	@ClearActionInterceptor
	@ActionInterceptor(MethodInterceptor.class)
	@ActionMapping
	public String method3(String a) {
		System.out.println(getClass().getName() + ".method3(int)");
		return a;
	}

	/**
	 * 没有ActionMapping注解，这个方法不会被注册，无法通过web访问
	 */
	public void method4() {
		System.out.println(getClass().getName() + ".method4()");
	}
}
```
### 编写拦截器

```java
public class MethodInterceptor extends AbstractActionIntercptor {
	@Override
	public void interceptor(ActionInvoker invoker) {
		System.out.println(getClass().getName()+":in");
		invoker.inovke();
		System.out.println(getClass().getName()+":out");
	}
}
```
### 启动服务

com.ditty.demo.server.ServerDemo.java

```java
public class ServerDemo {
	public static void main(String[] args) {
		//注册单个Controller类，不开启类扫描器时使用
		//HttpConfiguration.me().getRouters().add(Controller.class);
		//开启类扫描器，配置类扫描器的class过滤前缀 和Jar包过滤前缀
		HttpConfiguration.me().getClassSaner().addClassPrefixs("com.ditty.demo.").addJarPrefixs("").scanClass();
		//开启类扫描器，不配置扫描前缀时为全量扫描，性能没有配置过滤前缀的块
		//HttpConfiguration.me().getClassSaner().scanClass();
		ServerFactory.me().getServer().start();
		//自定义服务器启动
		//ServerFactory.me().getServer().start(webDir, port, contextPath);
	}
}
```
*注：支持自定义类扫描，扫描到带`Controller`注解类自动注册其路由信息*

### web访问

#### method1

```http
POST localhost:8090/controller/method1
{
	"b":{"a":1,"b":true,"d":"2018-05-26 16:36:34.039","s":"ss"}
}
```



```json
{
  "returnValue": {
    "a": 1,
    "b": true,
    "bBean": {
      "a": 1,
      "b": true,
      "d": "2018-05-26 16:36:34.039",
      "s": "ss"
    },
    "s": "A"
  }
}
```

#### method2

```http
POST localhost:8090/controller/method2
{
	"0":1
}
```

```json
{
  "returnValue": 1
}
```

#### method3

```http
POST localhost:8090/controller/method3/cc
```
*注：路径参数使用方式：url路径尾部`/`追加参数值，多个参数值以`-`为分隔* 

```http
POST localhost:8090/controller/method3?0=cc
```

```json
{
  "returnValue": "cc"
}
```
*注：Query参数使用方式：url路径尾部`?`追加参数赋值表达式`arg0=arg0Value`，多个表达式以`&`分隔*

#### 静态资源访问

```
GET localhost:8090/index.html
```

*注：静态资源必须包含`.`*

## 更新日志

2018-07-17

- 缓存Controller实例

2018-06-22

- 注册路由信息的类上需要有的注解修改ActionMapping->Controller
- 删除ClearAction注解类

2018-06-20

- jdk1.7->jdk1.6

2018-05-31

- 支持自定义类扫描，扫描到带ActionMapping注解类自动注册其路由信息
- 注册路由信息的类，类上需要有ActionMapping注解

2018-05-28

-  jdk1.8->jdk1.7 

2018-05-27

- ActionMapping注解支持Http各种方法：GET/POST/PUT/DELETE等
- 支持http url query的传参方式
  localhost:8090/controller/method?arg0=x0&arg2=x2

## 交流

请在下方评论或提issue，我会及时回复。

## 打赏

如果感觉本项目对您有用，打赏是对作者最大的鼓励。

![](https://note.youdao.com/yws/api/personal/file/9C97D8BD1E9E497097DEC70A1E739090?method=download&shareKey=90d5b78980e32ff73fde6c3035e34a75)