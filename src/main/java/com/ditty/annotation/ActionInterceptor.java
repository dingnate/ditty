/**
 * 
 */
package com.ditty.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.ditty.interceptor.AbstractActionIntercptor;

/**
 * @author dingnate
 *
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ActionInterceptor {
	Class<? extends AbstractActionIntercptor>[] value();
}
