/**
 * 
 */
package com.ditty.clazz;

import com.ditty.router.Routers;

/**
 * @author dingnate
 *
 */
public class ActionMappingClassHandler implements IClassHandler {
	@Override
	public void handle(Class<?> clazz) {
		Routers.me().add(clazz);
	}
}
