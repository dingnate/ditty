/**
 * 
 */
package com.ditty.clazz;

/**
 * @author dingnate
 *
 */
public interface IClassHandler {
	/**
	 * 处理类
	 * 
	 * @param clazz
	 */
	void handle(Class<?> clazz);
}
