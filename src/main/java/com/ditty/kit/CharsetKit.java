/**
 * 
 */
package com.ditty.kit;

import java.nio.charset.Charset;

/**
 * 字符编码工具
 * 
 * @author dingnate
 *
 */
public class CharsetKit {
	/**
	 * UTF-8编码字符串
	 */
	public static final String STR_UTF_8 = "UTF-8";
	/**
	 * UTF-8编码
	 */
	public static final Charset UTF_8 = Charset.forName(STR_UTF_8);
	/**
	 * 默认编码字符串
	 */
	public static final String STR_DEFAULT_CHARSET = System.getProperty("default.charset", STR_UTF_8);
	/**
	 * 默认编码
	 */
	public static final Charset DEFAULT_CHARSET = Charset.forName(STR_DEFAULT_CHARSET);

	CharsetKit() {
	}

}
