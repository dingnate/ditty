/**
 * 
 */
package com.ditty.kit;

/**
 * @author dingnate
 *
 */
public class HttpKit {
	public static final int STATUS_200 = 200;
	public static final int STATUS_404 = 404;
	public static final String EXT_HTML = "html";
	public static final String EXT_HTM = "htm";
	public static final String EXT_XHTML = "xhtml";
	public static final String EXT_XML = "xml";
	public static final String EXT_JSON = "json";
	public static final String HEADER_CONTENTTYPE = "content-type";
	public static final String CONTENTTYPE_TEXT_PLAIN = "text/plain";
	public static final String CONTENTTYPE_TEXT_HTML = "text/html";
	public static final String CONTENTTYPE_TEXT_XML = "text/xml";
	public static final String CONTENTTYPE_APPLICATION_OCTET_STREAM = "application/octet-stream";
	public static final String CONTENTTYPE_APPLICATION_JSON = "application/json";
	public static final String METHOD_GET = "GET";
	public static final String METHOD_POST = "POST";
	public static final String METHOD_PUT = "PUT";
	public static final String METHOD_DELETE = "DELETE";

	public static String getContentType(String ext) {
		String contentType;
		if (EXT_HTML.equals(ext) || EXT_HTM.equals(ext) || EXT_XHTML.equals(ext)) {
			contentType = CONTENTTYPE_TEXT_HTML;
		} else if (EXT_XML.equals(ext)) {
			contentType = CONTENTTYPE_TEXT_XML;
		} else if (EXT_JSON.equals(ext)) {
			contentType = CONTENTTYPE_APPLICATION_JSON;
		} else {
			contentType = CONTENTTYPE_TEXT_PLAIN;
		}
		return contentType;
	}
}
