/**
 * 
 */
package com.ditty.kit;

/**
 * @author dingnate
 *
 */
public class ArrayKit {
	/**
	 * 获取object在array中的坐标
	 * 
	 * @param array
	 * @param object
	 * @return
	 */
	public static int indexOf(Object[] array, Object object) {
		for (int i = 0; i < array.length; i++) {
			if (array[i].equals(object)) {
				return i;
			}
		}
		return -1;
	}
}
