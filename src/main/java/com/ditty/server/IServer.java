/**
 * 
 */
package com.ditty.server;

/**
 * @author dingnate
 */
public interface IServer {

	/**
	 * start
	 */
	void start();

	/**
	 * start
	 * @param webDir
	 * @param port
	 * @param contextPath
	 */
	void start(String webDir, int port, String contextPath);

	/**
	 * stop
	 */
	void stop();
}
