/**
 * 
 */
package com.ditty.serialize;

import com.ditty.serialize.fastjson.FastJsonSerializeConvertor;

/**
 * @author dingnate
 *
 */
public class SerializeConvertorFactory {
	public static ISerializeConvertor getSerializeConvertor() {
		return new FastJsonSerializeConvertor();
	}
}
