/**
 * 
 */
package com.ditty.wrap;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

/**
 * @author dingnate
 *
 */
public interface IHttpResponseWrapper {
	/**
	 * 获取headers
	 * @return 
	 */
	Map<String, String> headers();

	/**
	 * 获取statusCode
	 * @return
	 */
	int getStatusCode();

	/**
	 * 设置statusCode
	 * @param statusCode
	 */
	void setStatusCode(int statusCode);

	/**
	 * 写入字节数组
	 * @param bytes
	 * @throws IOException
	 */
	void write(byte[] bytes) throws IOException;

	/**
	 * 写入流
	 * @param is
	 * @throws IOException
	 */
	void write(InputStream is) throws IOException;

	/**
	 * 获取输出流
	 * @return
	 */
	OutputStream getOutputStream();

	/**
	 * 获取content
	 * @return
	 */
	byte[] content();
}
