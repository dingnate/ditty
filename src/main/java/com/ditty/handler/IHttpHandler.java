package com.ditty.handler;

import com.ditty.invoke.handler.HandlerInvoker;

/**
 * @author dingnate
 *
 */
public interface IHttpHandler {
	/**
	 * handle
	 * @param invoker
	 */
	void handle(HandlerInvoker invoker);
}
