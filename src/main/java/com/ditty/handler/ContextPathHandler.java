/**
 * 
 */
package com.ditty.handler;

import com.ditty.invoke.handler.HandlerInvoker;

/**
 * @author dingnate
 *
 */
public class ContextPathHandler extends AbstractHttpHandler {
	String contextPath = "";

	public ContextPathHandler(String contextPath) {
		this.contextPath = contextPath;
	}

	@Override
	public void handle(HandlerInvoker invoker) {
		if (!invoker.getTarget().startsWith(contextPath)) {
			return;
		}
		invoker.setTarget(invoker.getTarget().substring(contextPath.length())).handle();
	}
}
