/**
 * 
 */
package com.ditty.invoke.handler;

import com.ditty.wrap.IHttpRequestWrapper;
import com.ditty.wrap.IHttpResponseWrapper;

/**
 * @author dingnate
 *
 */
public interface IHandlerInvoker {
	/**
	 * 获取HttpRequestWrapper
	 * @return
	 */
	IHttpRequestWrapper getHttpRequestWrapper();

	/**
	 * 获取HttpResponseWrapper
	 * @return
	 */
	IHttpResponseWrapper getHttpResponseWrapper();

	/**
	 * handle
	 * @return 
	 */
	IHandlerInvoker handle();
}
