package com.ditty.demo.bean;

/**
 * @author dingnate
 *
 */
public class AxBean {
	int a;
	boolean b;
	public AxBean() {
	}
	/**
	 * @return the a
	 */
	public final int getA() {
		return a;
	}
	/**
	 * @param a the a to set
	 */
	public final void setA(int a) {
		this.a = a;
	}
	/**
	 * @param a
	 * @param b
	 * @param s
	 * @param bBean
	 */
	public AxBean(int a, boolean b, String s, BxBean bBean) {
		super();
		this.a = a;
		this.b = b;
		this.s = s;
		this.bBean = bBean;
	}
	/**
	 * @return the b
	 */
	public final boolean isB() {
		return b;
	}
	/**
	 * @param b the b to set
	 */
	public final void setB(boolean b) {
		this.b = b;
	}
	/**
	 * @return the s
	 */
	public final String getS() {
		return s;
	}
	/**
	 * @param s the s to set
	 */
	public final void setS(String s) {
		this.s = s;
	}
	/**
	 * @return the bBean
	 */
	public final BxBean getbBean() {
		return bBean;
	}
	/**
	 * @param bBean the bBean to set
	 */
	public final void setbBean(BxBean bBean) {
		this.bBean = bBean;
	}
	String s;
	BxBean bBean;
}
