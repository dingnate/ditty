/**
 * 
 */
package com.ditty.demo.controller;

import com.ditty.annotation.ActionInterceptor;
import com.ditty.annotation.ActionMapping;
import com.ditty.annotation.ClearActionInterceptor;
import com.ditty.annotation.Controller;
import com.ditty.demo.bean.AxBean;
import com.ditty.demo.bean.BxBean;
import com.ditty.demo.interceptor.ControllerInterceptor;
import com.ditty.demo.interceptor.MethodInterceptor;
import com.ditty.kit.HttpKit;

/**
 * 注册路由信息的类，类上需要有{@link Controller}注解
 * @author dingnate
 *
 */
@ActionInterceptor(ControllerInterceptor.class)
@ActionMapping(actionKey = "/controller")
@Controller
public class DemoController {
	/**
	 * httpMethod指定http的请求方式
	 * argName指定参数名可以在web访问的时候使用<br>
	 * actionKey:控制器的actionKey和方法的actionKey确定该方法的唯一路由路径<br>
	 * 不指定方法的actionKey默认为方法名称
	 * @param b
	 * @return
	 */
	@ActionMapping(httpMethod = HttpKit.METHOD_POST, actionKey = "method1", argNames = { "b" })
	public AxBean method1(BxBean b) {
		System.out.println(getClass().getName() + ".method1(BBean)");
		return new AxBean(1, true, "A", b);
	}

	/**
	 * 本方法的拦截器不仅包含在本方法上声明的拦截器，还包含上层控制器的拦截器、全局拦截器
	 * @param a
	 * @return
	 */
	@ActionInterceptor(MethodInterceptor.class)
	@ActionMapping(argNames = { "a" })
	public int method2(int a) {
		System.out.println(getClass().getName() + ".method2(int)");
		return a;
	}

	/**
	 * 这个方法清除了上层拦截器，保留本方法的拦截器<br>
	 * 通过web访问时，参数名可以用下表代替，比如 “a”->"0"
	 * @param a
	 * @return
	 */
	@ClearActionInterceptor
	@ActionInterceptor(MethodInterceptor.class)
	@ActionMapping
	public String method3(String a) {
		System.out.println(getClass().getName() + ".method3(int)");
		return a;
	}

	/**
	 * 没有ActionMapping注解，这个方法不会被注册，无法通过web访问
	 */
	public void method4() {
		System.out.println(getClass().getName() + ".method4()");
	}
}