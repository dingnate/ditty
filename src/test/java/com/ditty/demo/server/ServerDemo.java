/**
 * 
 */
package com.ditty.demo.server;

import com.ditty.HttpConfiguration;
import com.ditty.server.ServerFactory;

/**
 * @author dingnate
 *
 */
public class ServerDemo {
	public static void main(String[] args) {
		//注册单个Controller类，不开启类扫描器时使用
		//HttpConfiguration.me().getRouters().add(Controller.class);
		//开启类扫描器，配置类扫描器的class过滤前缀 和Jar包过滤前缀
		HttpConfiguration.me().getClassSaner().addClassPrefixs("com.ditty.demo.").addJarPrefixs("").scanClass();
		//开启类扫描器，不配置扫描前缀时为全量扫描，性能没有配置过滤前缀的块
		//HttpConfiguration.me().getClassSaner().scanClass();
		ServerFactory.me().getServer().start();
		//自定义服务器启动
		//ServerFactory.me().getServer().start(webDir, port, contextPath);
	}
}
